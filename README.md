# Stack 💐 Vercel + Airtable + Next 💐

**Vercel**

Bootstrap project with Next template and opt in for gitlab integration:

Each commit to main/master will trigger a deploy to production.
Each commit to another branch will trigger a preview deploy URL.

From vercel.com dashboard or CLI we can add .env variables for each production,preview and development (ie: Airtable API-KEY)

**Next**

Routing follows file system, underscore'd files are a way to customize defaults provided by the framework:

/pages
/api
\_app.js
\_head.js
\_document.js
404.js
index.js
legal.js

Both 'pages' and 'api' shouldn't be renamed.

Api folder contains 4 lambda functions for each of Airtable's interaction. (Hint: check vercel.json)
Each lambda must contain all it needs, no shared 'state' between them or abstraction (ie: a global Airtable configuration). Each form on the client call it's pertinent api route who in turn save user data to Airtable.

**Airtable**

Cap of 5 requests/second. 4 'bases' created, login to airtable.com with these credentials and export data:

pmones@wideagency.com
20airtable20

## Get started

```bash
vercel dev
```

last edit: 20/08/20
blame: pol moneys
