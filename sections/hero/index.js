import { useState } from 'react';
import { calcTimeDelta } from 'react-countdown';
import dynamic from 'next/dynamic';
import { Display } from './display';
import { Spacer } from '../../units/box';

const GOAL_DATE = '2020-10-08T00:01:00'; // Date.now() + 5000;

const SafeSSRCountdown = dynamic(
  () => import('react-countdown').then((mod) => mod),
  {
    loading: () => <p>Loading...</p>,
    ssr: false,
  },
);

const HeroSection = ({ locale = 'FR' }) => {
  const [stats, _] = useState(() => timeRemaining(GOAL_DATE));

  return (
    <aside id="hero" className="-blanc _violet">
      <Spacer.MQ size="xl" />
      <Spacer.MQ size="xl" />
      <div className="mq-only flex col main-end">
        <h2 className="h2 _noir pxy -sm">QLIK DAY GENEVA</h2>
        <p className="_bleu pxy -sm -noir self-push-left font-bold">
          {' '}
          08 OCTOBRE 2020
        </p>
      </div>
      <Spacer size="l" />
      <SafeSSRCountdown
        date={GOAL_DATE}
        renderer={(props) => <Display {...props} locale={locale} />}
      />
      <p role="timer" className="offscreen">
        {stats.days} days left to the event
      </p>
    </aside>
  );
};

function timeRemaining() {
  const { total, days, hours, minutes, seconds, completed } = calcTimeDelta(
    GOAL_DATE,
  );
  return { total, days, hours, minutes, seconds, completed };
}

export { HeroSection };
