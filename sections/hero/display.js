import { zeroPad } from 'react-countdown';
// import { PostCountdown } from "./post";
import { Col, Row } from '../../units/box';
import TRANSLATIONS from './locale';

const Display = ({
  days,
  hours,
  minutes,
  seconds,
  completed,
  locale = 'FR',
}) => {
  // if (completed) return  <PostCountdown />;
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <Row className=" row-gap countdown " aria-hidden="true">
      <DisplayCard>
        <p className="h2 -noir"> {zeroPad(days)}</p>
        <p className="font-s font-uppercase -noir">{translate('day')}</p>
      </DisplayCard>
      <DisplayCard>
        <p className="h2 -noir"> {zeroPad(hours)}</p>
        <p className="font-s font-uppercase -noir">{translate('hour')}</p>
      </DisplayCard>
      <DisplayCard>
        <p className="h2 -noir"> {zeroPad(minutes)}</p>
        <p className="font-s font-uppercase -noir"> {translate('min')}</p>
      </DisplayCard>
      <DisplayCard>
        <p className="h2 -noir"> {zeroPad(seconds)}</p>
        <p className="font-s font-uppercase -noir"> {translate('sec')}</p>
      </DisplayCard>
    </Row>
  );
};

const DisplayCard = ({ children, isFeatured = false }) => {
  const clss = isFeatured
    ? ' self cross-center -noir'
    : ' self cross-center -blanc';
  return <Col className={clss}> {children} </Col>;
};

// const DisplayCardFeatured = (props) => <DisplayCard isFeatured {...props} />;

export { Display };
