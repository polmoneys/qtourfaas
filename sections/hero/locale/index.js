const EN = {
  day: 'DAYS',
  hour: 'HOURS',
  min: 'MINUTES',
  sec: 'SECONDS',
};

const FR = {
  day: 'JOURNÉES',
  hour: 'HEURES',
  min: 'MINUTES',
  sec: 'SECONDES',
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
