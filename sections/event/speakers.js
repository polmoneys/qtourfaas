import { Pic } from '../../units/pic';
import { Ico } from '../../units/icon';
import { Col, Row, Spacer } from '../../units/box';
import TRANSLATIONS from './locale/speakers';

const Speakers = ({ locale = 'FR' }) => {
  const SPEAKERS_LIST = TRANSLATIONS[locale];

  return (
    <Col tag="section" className="section-l">
      <p className="h2 font-left">LES ORATEURS</p>
      <Spacer />
      <div id="speakers" className="flex grid">
        {SPEAKERS_LIST.map((speaker) => (
          <Card {...speaker} key={speaker.id} locale={locale} />
        ))}
        <Card placeholder />
      </div>
    </Col>
  );
};

const Card = ({ portrait, name, bio, role, linkedin, placeholder = false }) => {
  const clss = placeholder
    ? 'speaker-card main-start pxy placeholder'
    : 'speaker-card main-start pxy';
  if (placeholder) return <Col className={clss}></Col>;
  return (
    <Col className={clss}>
      <Row className=" main-between">
        <Pic.AvatarXL url={portrait} />
        <a href={linkedin} target="_blank" rel="noopener noreferrer">
          <Ico color="var(--blanc)" s />
          <span className="offscreen">Go to {name} Linkedin</span>
        </a>
      </Row>
      <p className="h2 font-bold">{name}</p>
      <p className="font-s">{role}</p>
      <Spacer />
      <p>{bio}</p>
    </Col>
  );
};

export { Speakers };
