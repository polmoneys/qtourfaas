import { Col, Row, Spacer } from '../../units/box';
import TRANSLATIONS from './locale/program';

const Program = ({ locale = 'FR' }) => {
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <Col tag="section" id="program" className="section-l -blanc ">
      <p className="h2 font-left">{translate('title')}</p>

      <ProgramRow>
        <time dateTime="2022-10-08T09:15:00">9:15</time>
        <div>
          <p className="h2 font-left">{translate('moment')}​</p>
          <p>{translate('moment_d')}</p>
        </div>
      </ProgramRow>
      <ProgramRow>
        <time dateTime="2022-10-08T10:00:00">10:00</time>
        <div>
          <p className="h2 font-left">{translate('moment2')}</p>
          <p>{translate('moment2_d')}</p>
        </div>
      </ProgramRow>
      <ProgramRow>
        <time dateTime="2022-10-08T12:30:00">12:30</time>
        <div>
          <p className="h2 font-left">{translate('moment3')}</p>
          <p>{translate('moment3_d')} </p>
        </div>
      </ProgramRow>

      <ProgramRow>
        <time dateTime="2022-10-08T14:00:00">14:00</time>
        <div>
          <p className="h2 font-left">{translate('cta')}</p>
          <Spacer size="xl" />

          <p className="font-bold">WORKSHOP #1 - {translate('workshop_t')} </p>
          <p>{translate('workshop')}</p>
          <Spacer />
          <p>{translate('workshop_d')}</p>
          <Spacer size="l" />
          <p className="font-bold">WORKSHOP #2 - {translate('workshop2_t')} </p>
          <p>{translate('workshop2')}</p>
          <Spacer />
          <p>{translate('workshop2_d')}</p>
          <Spacer size="xl" />
          <p className="font-bold">FORMATION #1 - {translate('formation_t')}</p>
          <p>{translate('formation')}</p>
          <Spacer />

          <p>{translate('formation_d')}</p>
          <Spacer size="l" />
          <p className="font-bold">
            FORMATION #2 - {translate('formation2_t')}
          </p>
          <p>{translate('formation2')}</p>
          <Spacer />

          <p>{translate('formation2_d')}</p>
        </div>
      </ProgramRow>

      <ProgramRow>
        <time dateTime="2022-10-08T16:00:00">16:00</time>
        <div>
          <p className="h2 font-left">{translate('moment4')}</p>
          <p>{translate('moment4_d')}</p>
        </div>
      </ProgramRow>
      <Spacer size="xl" />

      {/* <a
        href=""
        target="_blank"
        rel="noopener noreferer"
        className="action -blanc traced font-center action-landscape-max-width"
      >
               {translate("button")}
      </a> */}
    </Col>
  );
};

const ProgramRow = ({ children }) => (
  <Row className="program-row main-between cross-baseline">{children}</Row>
);

export { Program };
