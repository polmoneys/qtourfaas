const EN = {
  title: 'SCHEDULE',
  moment: 'WELCOME AND BREAKFAST',
  moment_d:
    'Start the day with breakfast and use the time to chat with our consultants and your fellow attendees.',
  moment2: 'PLENARY SESSION',
  moment2_d:
    'A variety of keynotes will be presented by CROSS and Qlik consultants during the plenary session exploring data visualisation and data use in business.',
  moment3: 'LUNCH BREAK',
  moment3_d:
    'Due to COVID-19 and safety measures in place, we cannot provide lunch on our premises.',
  moment4: 'AFTER WORK EVENT​',
  moment4_d:
    'To end the day, we would like you to join us for a drink at a venue outside our premises to debrief and discuss the day.',
  button: 'Download program',
  cta: 'WORKSHOPS AND COURSES',
  workshop:
    'Augmented intelligence - In-depth expertise and decisions based on data for everyone.',
  workshop_t: 'Duration: 45mins',
  workshop_d:
    "The workshop will demonstrate how Qlik Sense's Insight Advisor tool provides a range of analyses and data generating functions to bring you more in-depth and meaningful information based on data. Visual analysis provides better data visualisation and better insights based on all the data and search criteria in natural language, which users can fine-tune and add to dashboards for a more in-depth exploration.",
  workshop2: 'Qlik Data Calatyst',
  workshop2_t: 'Duration: 45mins',
  workshop2_d:
    'Qlik Data Catalyst creates a secure library for the company to provide users with a unique and easy-to-use catalogue. It allows users to understand and access any source of company data.',
  formation:
    'How do I use modelling to harness all the power of the associative engine?',
  formation_t: 'Duration 1 hr (premium only)',
  formation_d:
    'Learn to use different data models. Understand how to solve big data model issues. Create a data model to handle large volumes of data.',
  formation2:
    ' Data Visualisation: Unleash data to get your work back on track!',
  formation2_t: 'Duration 1 hr (premium only)',
  formation2_d:
    "How do I intelligibly and pragmatically address the 20% of business needs that require 80% of the data volume in my QlikSense app? That's the question we shall attempt to answer by implementing the 'Dynamic View' and 'ODAG' modules in Qlik Sense.",
};

const FR = {
  title: 'PROGRAMME',
  moment: 'ACCUEIL ET PETIT-DEJEUNER',
  moment_d:
    'Commencez la journée avec un petit-déjeuner et profitez de ce temps pour échanger avec nos experts, ainsi que les autres participants. ',
  moment2: 'PLENIERE',
  moment2_d:
    'Lors de cette plénière, différentes keynotes vous seront présentées par les experts CROSS et Qlik, au sujet de la data visualisation et de l’utilisation de la données au sein des entreprises.',
  moment3: 'LUNCH BREAK',
  moment3_d:
    'En raison du COVID-19 et des gestes barrières à respecter, nous ne pourrons vous proposer un déjeuner au sein de nos locaux.',
  moment4: 'AFTERWORK​',
  moment4_d:
    'À la fin de cette journée, nous vous proposerons de prendre un verre ensemble dans un lieu extérieur à nos locaux pour débriefer et échanger sur la journée écoulée. ',
  button: 'télécharger le PROGRAMME',
  cta: 'WORKSHOPS ET FORMATIONS​',
  workshop:
    'Intelligence augmentée - Des connaissances plus approfondies et des décisions fondées sur des données pour tous.',
  workshop_t: 'Durée: 45mins',
  workshop_d:
    "Ce workshop démontrera comment l’outil Insight Advisor de Qlik Sense fournit une gamme d'analyses et de capacités de génération d'informations qui permettent d'obtenir des informations plus approfondies et plus significatives à partir des données. L'analyse visuelle génère les meilleures visualisations de données et les meilleurs aperçus en fonction de l'ensemble des données et des critères de recherche en langage naturel, que les utilisateurs peuvent affiner et ajouter aux tableaux de bord pour une exploration plus approfondie.",
  workshop2: 'Qlik Data Calatyst',
  workshop2_t: 'Durée: 45mins',
  workshop2_d:
    "Qlik Data Catalyst crée une bibliothèque sécurisée à l'échelle de l'entreprise, afin de fournir aux utilisateurs un catalogue unique et facile à utiliser. Il permet de comprendre et accéder à toute source de données d'entreprise.",
  formation:
    'Comment utiliser toute la puissance du moteur associatif grâce à la modélisation ?',
  formation_t: 'Durée: 1h (Premium uniquement)',
  formation_d:
    'Apprendre à utiliser les différents modèles de données. Comprendre comment résoudre les problèmes des grands modèles de données. Créer un modèle de données pour exploiter les grands volumes de données.',
  formation2:
    ' Data Visualisation: Relancez votre activité en libérant la donnée !',
  formation2_t: 'Durée: 1h (Premium uniquement)',
  formation2_d:
    "Comment adresser intelligemment et de manière pragmatique ces 20% de besoin Métier qui nécessitent 80% du volume de données dans mon application QlikSense ? Telle est la problématique à laquelle nous tâcherons de répondre par la mise en application, dans Qlik Sense, des modules 'Dynamic View' et 'ODAG.'",
};
const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
