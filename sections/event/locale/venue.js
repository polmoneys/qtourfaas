const EN = {
  title: 'USEFUL INFORMATION',
  intro:
    "We'll see you on October 8th 2020 at CROSS's premises in Geneva at the following address:",
  address: '3rd floor',
  button: 'OPEN MAPS',
  train_t: 'Train',
  train_d:
    "If you're travelling by train, the Lancy-Pont-Rouge stop is a 5 minute walk from our premises.",
  tram_t: 'Tram',
  tram_d:
    'The Lancy-Pont-Rouge-Gare/Étoile bus and tram stop is a 2 minute walk from our premises. Direct journeys on tram lines 15 and 17 and bus routes 21, 43, D, J and K.',
  car_t: 'CAR',
  car_d:
    "You can park at the P+R Étoile car park, a 2 minute walk from the CROSS premises. There are pay and display spaces across the road from our premises on Route des Acacias 45B 1227 Acacias. We provide free parking spaces in our office basement for 'Premium' attendees. You will be sent your parking space number at a later date.",
};

const FR = {
  title: 'INFORMATIONS PRATIQUES',
  address: '3ème étage',
  intro:
    'Nous vous donnons rendez-vous le 08 octobre 2020 dans les locaux de CROSS à Genève, situés à l’adresse suivante:',
  button: 'OUVRIR MAPS',
  train_t: 'Train',
  train_d:
    'Pour les personnes voyageant en train, l’arrêt Lancy-Pont-Rouge se situe à 5 minutes à pied de nos locaux.',
  tram_t: 'Tram',
  tram_d:
    'L’arrêt de bus et tram Lancy-Pont-Rouge-Gare / Étoile se situe à 2 minutes à pied de nos locaux. Les lignes de tram 15 et 17 s’y rendent directement, ainsi que les bus 21, 43, D, J et K.',
  car_t: 'Voiture',
  car_d:
    'Vous avez la possibilité de déposer vos véhicules au Parking P+R Étoile qui se situe à 2 minutes à pied des locaux de CROSS. Quelques places de parking payantes sont disponibles en face de nos bureaux, à la route des Acacias 45B 1227 Acacias. Pour les participants ayant des entrées « Premium », des places de parking sont à votre disposition gratuitement au sous-sol de nos locaux. Votre numéro de place vous sera communiqué ultérieurement.',
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
