const EN = {
  key1:
    "Are your reports no longer giving you an understanding of what you're doing? It's high time to bring the dataviz revolution to your workplace. Data visualisation isn't the trendy management accessory whose colours change from one season to the next. It's a powerful analytics tool based on cognitive concepts that, here at CROSS and Qlik, we believe is the key to unlocking your data and making it intelligent and intelligible.",
  key2:
    "Dive into data for the day with Qlik and the CROSS teams at CROSS's premises in Geneva on October 8th and learn how to turn your data into value, make it easy for everyone to understand and ensure it actually fulfils your professional needs!",
  key3: 'Find out more about',
  seats: 'participants',
  speakers: 'speakers',
  length: 'dedicated day',
  keynote: 'keynotes',
  workshops: 'workshops',
  formations: 'courses',
};

const FR = {
  key1:
    'Vos reportings ne vous permettent plus de comprendre votre activité ? Il est grand temps d’initier la révolution dataviz au sein de vos métiers. La data visualisation n’est pas l’accessoire de mode du pilotage dont les couleurs changeraient d’une saison à l’autre. Puissant outil analytics basé sur les concepts cognitifs, nous sommes convaincus chez CROSS et Qlik qu’il s’agit de la clé pour vous approprier vos données et les rendre intelligentes et intelligibles.',
  key2:
    'A travers cette journée 100% Data qui aura lieu le 08 octobre aux côtés de Qlik et des équipes CROSS dans les locaux de CROSS à Genève, apprenez à libérer la valeur de la donnée, démocratisez son accès et faire en sorte qu’elle réponde enfin à vos interrogations métier !',
  key3: 'En savoir plus sur',
  seats: 'participants',
  speakers: 'orateurs',
  length: 'jour dédié',
  keynote: 'keynotes',
  workshops: 'workshops',
  formations: 'formations',
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
