import { Speakers } from './speakers';
import { Program } from './program';

function EventSection({ locale = 'FR' }) {
  return (
    <>
      <Speakers locale={locale} />
      <div className="_noir">
        <Program locale={locale} />
      </div>
    </>
  );
}

export { EventSection };
