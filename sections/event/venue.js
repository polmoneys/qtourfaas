import { Col, Spacer } from '../../units/box';
import TRANSLATIONS from './locale/venue';

const VenueSection = ({ locale = 'FR' }) => {
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <Col tag="section" className="section-l ">
      <p className="h2 font-bold"> {translate('title')}</p>

      <Spacer />
      <p>{translate('intro')}</p>
      <Spacer />
      <address>
        Route des Acacias 45B <LineBreak />
        1227 Genève
        <LineBreak />
        {translate('address')}
      </address>
      <Spacer size="l" />

      <p className="font-bold"> {translate('train_t')}</p>

      <p>{translate('train_d')}</p>
      <Spacer />
      <p className="font-bold"> {translate('tram_t')}</p>

      <p>{translate('tram_d')}</p>
      <Spacer />
      <p className="font-bold"> {translate('car_t')}</p>

      <p>{translate('car_d')}</p>
      <Spacer size="l" />
      <a
        href="https://www.google.es/maps/place/Route+des+Acacias+45B,+1227+Gen%C3%A8ve,+Switzerland/@46.1889485,6.1266968,17z/data=!3m1!4b1!4m5!3m4!1s0x478c7b2f87a97065:0xb19ba27b69c05ad2!8m2!3d46.1889485!4d6.1288908"
        target="_blank"
        rel="noopener noreferer"
        className="action -violet traced font-center action-landscape-max-width"
      >
        {translate('button')}
      </a>
    </Col>
  );
};

const LineBreak = () => <br aria-hidden="true" />;

export { VenueSection };
