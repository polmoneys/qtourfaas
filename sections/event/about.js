import AnimatedNumber from 'animated-number-react';
import { Col, ColToRow, Grid, Spacer } from '../../units/box';
import TRANSLATIONS from './locale/about';

const AboutSection = ({ locale = 'FR' }) => (
  <>
    <Col tag="section" className="section-l owl">
      <Intro locale={locale} />
    </Col>
    <Spacer />
    <div className="_bleu">
      <StatsBar locale={locale} />
    </div>
  </>
);

const Intro = ({ locale = 'FR' }) => {
  const translate = (goal) => TRANSLATIONS[locale][goal];
  return (
    <>
      <ColToRow className="main-between cross-center">
        <h1 className="h1 font-bold">QLIK DAY GENEVA</h1>
        <p className="font-bold -violet"> #qlikdaygeneva</p>
      </ColToRow>
      <p>{translate('key1')}</p>
      <p>{translate('key2')}</p>
      <p>
        {translate('key3')}{' '}
        <a
          href="https://www.cross-systems.ch/"
          target="_blank"
          rel="noreferer noopener"
          className="link-underline"
        >
          CROSS
        </a>{' '}
        et{' '}
        <a
          href="https://www.qlik.com/"
          target="_blank"
          rel="noreferer noopener"
          className="link-underline"
        >
          Qlik
        </a>
        .
      </p>
    </>
  );
};

const ANIMATION = {
  duration: 4000,
  short: 2000,
  delay: 1200,
};
const ANIMATION_SHORT = {
  duration: 2000,
  delay: 1200,
};
const StatsBar = ({ locale = 'FR' }) => {
  const prettyNumber = (num) => num.toFixed(0);
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <Grid className="section-l -blanc">
      <StatCard>
        <p className="animated-stat font-bold">
          <AnimatedNumber
            formatValue={prettyNumber}
            {...ANIMATION}
            value={50}
          />
        </p>
        <p className="font-uppercase font-s"> {translate('seats')}</p>
      </StatCard>
      <StatCard>
        <p className="animated-stat font-bold">
          <AnimatedNumber
            formatValue={prettyNumber}
            {...ANIMATION_SHORT}
            value={5}
          />
        </p>
        <p className="font-uppercase font-s"> {translate('speakers')}</p>
      </StatCard>
      <StatCard>
        <p className="animated-stat font-bold">
          <AnimatedNumber
            formatValue={prettyNumber}
            {...ANIMATION_SHORT}
            value={1}
          />
        </p>
        <p className="font-uppercase font-s"> {translate('length')}</p>
      </StatCard>
      <StatCard>
        <p className="animated-stat font-bold">
          <AnimatedNumber
            formatValue={prettyNumber}
            {...ANIMATION_SHORT}
            value={2}
          />
        </p>{' '}
        <p className="font-uppercase font-s"> {translate('keynote')}</p>
      </StatCard>
      <StatCard>
        <p className="animated-stat font-bold">
          <AnimatedNumber
            formatValue={prettyNumber}
            {...ANIMATION_SHORT}
            value={2}
          />
        </p>
        <p className="font-uppercase font-s"> {translate('workshops')}</p>
      </StatCard>
      <StatCard>
        <p className="animated-stat font-bold">
          <AnimatedNumber
            formatValue={prettyNumber}
            {...ANIMATION_SHORT}
            value={2}
          />
        </p>
        <p className="font-uppercase font-s"> {translate('formations')}</p>
      </StatCard>
    </Grid>
  );
};

const StatCard = ({ children }) => (
  <Col className="stat-card self main-end cross-center">{children}</Col>
);

export { AboutSection };
