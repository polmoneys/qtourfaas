import { useState } from 'react';
import { CloudinaryContext } from 'cloudinary-react';
import { Action } from '../../units/action';
import { Spacer, Row } from '../../units/box';
import TRANSLATIONS from './locale/upload';
const sleep = (ms = 1000) => new Promise((res) => setTimeout(res, ms));

const openUploadWidget = (newOpts, callback) => {
  window.cloudinary.openUploadWidget({ ...newOpts, ...widgetOpts }, callback);
};

const FormUpload = ({ fileName, locale = 'FR' }) => {
  const [success, setSuccess] = useState(false);
  const [fail, setError] = useState(false);

  const beginUpload = (tag) => {
    openUploadWidget(
      {
        prepareUploadParams: function (cb, params) {
          cb({ public_id: fileName });
        },
      },
      (error, photos) => {
        if (!error) {
          if (photos.event === 'success') {
            sleep().then(() => {
              setSuccess(true);
            });
          }
        } else {
          setError(true);
        }
      },
    );
  };

  const translate = (goal) => TRANSLATIONS[locale][goal];

  if (fail) return <FormUploadError msg={translate('error')} />;
  if (success) return <FormUploadSuccess msg={translate('success')} />;

  return (
    <CloudinaryContext cloudName="qlik-tour-geneve">
      <div className="form- upload font-center">
        <p>{translate('key1')}</p>
        <p>{translate('key2')} </p>
        <Spacer />

        <Row className="self-stretch main-center">
          <Action className="traced -violet" onPress={beginUpload}>
            {translate('button')}
          </Action>
        </Row>
      </div>
    </CloudinaryContext>
  );
};

const FormUploadError = ({ msg }) => (
  <>
    <p className="font-center">{msg}</p>
    <Spacer />
  </>
);

const FormUploadSuccess = ({ msg }) => (
  <>
    <p className="font-center">{msg}</p>
    <Spacer />
  </>
);
export { FormUpload };

const widgetOpts = {
  cloudName: 'qlik-tour-geneve',
  uploadPreset: 'yndxfvrz',
  sources: ['local', 'camera', 'facebook', 'instagram'],
  showAdvancedOptions: false,
  cropping: false,
  multiple: false,
  // language: 'fr', TODO: text.json locale
  defaultSource: 'local',
  styles: {
    palette: {
      window: '#ffffff',
      sourceBg: '#f4f4f5',
      windowBorder: '#90a0b3',
      tabIcon: '#000000',
      inactiveTabIcon: '#555a5f',
      menuIcons: '#555a5f',
      link: '#0433ff',
      action: '#339933',
      inProgress: '#0433ff',
      complete: '#339933',
      error: '#cc0000',
      textDark: '#000000',
      textLight: '#fcfffd',
    },
    fonts: {
      default: null,
      'sans-serif': {
        url: null,
        active: true,
      },
    },
  },
};
