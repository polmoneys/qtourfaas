import { useState } from 'react';
import axios from 'axios';
import {
  useForm,
  Input,
  Form,
  CheckBox,
  isEmptyStr,
  FormField,
  hasArroba
} from '../../units/forms';
import { Action } from '../../units/action';
import { Spacer, Row } from '../../units/box';
import { FormUpload } from './upload';
import TRANSLATIONS from './locale/premium';

const initialState = {
  email: '',
  nom: '',
  prenom: '',
  societe: '',
  role: '',
  agree: false,
};

const Premium = ({ locale = 'FR' }) => {
  const [formStates, formActions] = useForm(() => initialState);
  const { awaiting, v, fail, ok, invalid } = formStates;

  const {
    updateForm,
    showFixes,
    setError,
    setSuccess,
    setAwaiting,
  } = formActions;

  const update = (e) => {
    const { target, persist } = e;
    if (persist) e.persist();
    const { name, value } = target;
    updateForm((prev) => ({ ...prev, [name]: value }));
  };

  async function persist(fields) {
    setAwaiting(true);
    try {
      const res = await axios.post('/api/premium', fields);
      if ((res.status = 200)) return setSuccess(true);
    } catch (error) {
      setError(true);
      setSuccess(false);
    } finally {
      setAwaiting(false);
    }
  }

  const action = () => {
    if (
      isEmptyStr(v.nom) ||
      isEmptyStr(v.email) ||
      !hasArroba(v.email) ||
      isEmptyStr(v.prenom) ||
      isEmptyStr(v.societe) ||
      isEmptyStr(v.role) ||
      !v.agree
    ) {
      return showFixes(true);
    } else {
      const { agree, ...userDetails } = v;
      persist(userDetails);
    }
  };

  const translate = (goal) => TRANSLATIONS[locale][goal];

  if (fail) return <PremiumError msg={translate('error')} locale={locale} />;
  if (ok)
    return (
      <PremiumSuccess
        msg={translate('success')}
        userEmail={v.email}
        locale={locale}
      />
    );

  return (
    <>
      <Form id="premium-form" onSubmit={action}>
        <FormField>
          <Input.Required
            initial={v.nom}
            name="nom"
            label={translate('name')}
            onChange={update}
            showFixes={invalid}
            locale={locale}
          />
          <Input.Required
            initial={v.prenom}
            name="prenom"
            label={translate('surname')}
            onChange={update}
            showFixes={invalid}
            locale={locale}
          />
        </FormField>
        <FormField>
          <Input.Email
            initial={v.email}
            name="email"
            label={translate('email')}
            onChange={update}
            showFixes={invalid}
            locale={locale}
          />
          <Input.Required
            initial={v.societe}
            name="societe"
            label={translate('company')}
            onChange={update}
            showFixes={invalid}
            locale={locale}
          />
        </FormField>
        <FormField>
          <Input.Required
            initial={v.role}
            name="role"
            label={translate('role')}
            onChange={update}
            showFixes={invalid}
            locale={locale}
          />
          <Spacer />
        </FormField>
        <FormField>
          <p className="font-s">{translate('msg')} </p>
          <Spacer />
        </FormField>
        <Spacer />

        <CheckBox
          initial={v.agree}
          name="agree"
          label="*"
          onChange={update}
          required
          showFixes={invalid}
        />
      </Form>
      <p className="font-s">{translate('confirm')} </p>
      <Spacer />
      <Row className="self-stretch main-center">
        <Action className="traced -violet" onPress={action} disabled={awaiting}>
          {translate('button')}
        </Action>
      </Row>
    </>
  );
};

const PremiumSuccess = ({ msg, userEmail = '', locale = 'FR' }) => (
  <>
    <Spacer />
    <FormUpload fileName={userEmail} locale={locale} />
    <Spacer />
    <p className="font-center">{msg}</p>
    <Spacer />
  </>
);

const PremiumError = ({ msg }) => (
  <>
    <p className="font-center">{msg}</p>
    <Spacer />
  </>
);

export { Premium };
