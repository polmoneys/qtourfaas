import { Action } from '../../units/action';
import { Col, ColToRow, Row } from '../../units/box';
import TRANSLATIONS from './locale/landing';

const Landing = ({ cb, locale = 'FR' }) => {
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <ColToRow className="main-between cross-start ">
      <Col className="ticket-card self owl cross-start main-center -blanc">
        <p className="h2 font-bold">{translate('title1')}</p>
        <p>{translate('key1')}</p>
        <p>{translate('key2')}</p>
        {/* <p>{translate('key3')}</p> */}
        <p>{translate('key4')}</p>
        <p>{translate('key5')}</p>

        <Row className="self-stretch main-center self-push-top">
          <Action
            className="action-transparent traced -blanc inverted"
            onPress={() => cb(1)}
          >
            {translate('button')}
          </Action>
        </Row>
      </Col>
      <Col className="ticket-card self owl cross-start main-center -blanc">
        <Row className="self-stretch main-between">
          <p className="h2 font-bold">{translate('title2')}</p>
          <p className="h2">750CHF</p>
        </Row>

        <p>{translate('key6')}</p>
        <p>{translate('key1')}</p>
        <p>{translate('key2')}</p>

        <p>{translate('key3')}</p>
        {/* <p>{translate('key7')}</p> */}

        <p>{translate('key8')}</p>
        <p>{translate('key5')}</p>

        <Row className="self-stretch main-center self-push-top">
          <Action
            className="action-transparent traced -blanc inverted "
            onPress={() => cb(2)}
          >
            {translate('button')}
          </Action>
        </Row>
      </Col>
    </ColToRow>
  );
};

export { Landing };
