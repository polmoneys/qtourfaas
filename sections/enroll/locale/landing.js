const EN = {
  title1: 'STANDARD ADMISSION',
  title2: 'PREMIUM ADMISSION',
  button: 'I want to attend',
  key1: 'Breakfast',
  key2: 'Plenary session',
  key3: 'Lunch with our consultants',
  key4: 'Workshops x2',
  key5: 'After work event',
  key6: 'Underground parking space',
  key7: 'Lunch with our consultants ',
  key8: 'Courses x2',
};

const FR = {
  title1: 'ENTREE STANDARD',
  title2: 'ENTREE PREMIUM',
  button: 'JE PARTICIPE',
  key1: 'Petit déjeuner',
  key2: 'Plenière',
  key3: 'Lunch avec nos experts',
  key4: 'Workshops x2',
  key5: 'Afterwork',
  key6: 'Place de parking au sous-sol',
  key7: 'Rendez-vous privé 20min',
  key8: 'Formations x2',
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
