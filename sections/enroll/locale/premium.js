const EN = {
  name: 'Name',
  surname: 'Surname',
  email: 'Email',
  company: 'Company',
  role: 'Role',
  workshops: 'Drag and drop your workshop attendance preferences',
  confirm: 'I accept my data to be collected',
  button: 'Enroll',
  success:
    'Your attendance is confirmed subject to availability, upon receipt of the confirmation email from our team.',
  error:
    "Unfortunately we can't process your request, please reach us at qliktourgeneve@cross-systems.com for more information.",
  msg: 'Our team will send shortly a confirmation email of your reservation.',
};

const FR = {
  name: 'Nom',
  surname: 'Prénom',
  email: 'Email',
  company: 'Societé',
  role: 'Poste Occupé',
  workshops:
    'Veuillez ordonner vos préférences de workshops en glissant déposant les éléments.',
  confirm: "J'accepte que mes données soient collectées et stockées",
  button: "Je m'inscris",
  success:
    'Votre inscription a bien été prise en compte. Nous vous donnons rendez-vous le 08 octobre 2020 à Genève dans les locaux de CROSS.',
  error:
    "Malheureusement nous n'avons pas pu sauvegarder votre demande, veuillez nous contacter à qliktourgeneve@cross-systems.com pour obtenir plus d'informations.",
  msg:
    'Votre inscription est valable sous reserve de place disponible, après l’envoi de l’email de confirmation de notre équipe.',
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
