const EN = {
  key1:
    "Experience an event using facial recognition with us. If you'd like to take part, import your profile photo!",
  key2: '*It will be deleted after the event.',
  button: 'Upload',
  success: 'Your photo has been successfully imported, thank you!',
  error:
    "Unfortunately we can't process your request, please reach us at qliktourgeneve@cross-systems.com for more information.",
};

const FR = {
  key1:
    'Notre événement vous proposera de vivre une expérience utilisant la reconnaissance faciale. Si vous souhaitez y participer, importez votre photo portrait!',
  key2: "*Celle-ci ne sera pas conservée après l'événement.",
  button: 'Telecharger',
  success: 'Votre photo a bien été importé, merci !',
  error:
    "Malheureusement, votre demande n'a pas abouti. N'hésitez pas à nous contacter à l'adresse email suivante : qliktourgeneve@cross-systems.com pour obtenir plus d'informations",
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
