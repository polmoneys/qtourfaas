import { useState } from 'react';
import { Tabs, TabList, Tab, TabPanels, TabPanel } from '@reach/tabs'; // ***
import { Landing } from './landing';
import { Standard } from './standard';
import { Premium } from './premium';
import { Col, Spacer } from '../../units/box';

function EnrollSection({ locale = 'FR' }) {
  const [tabIndex, setTabIndex] = useState(0);

  const handleTabsChange = (index) => {
    setTabIndex(index);
  };
  return (
    <Col tag="section" className="section-l">
      <Tabs
        className="lobotomized-owl "
        index={tabIndex}
        onChange={handleTabsChange}
      >
        <TabList>
          <Tab
            className="h2"
            style={{
              opacity: tabIndex != 0 ? 1 : 0,
            }}
          >
            &times;
          </Tab>
        </TabList>
        <h2 className="h2 font-bold font-center">{labels[locale][tabIndex]}</h2>
        <TabPanels>
          <TabPanel>
            <Landing cb={handleTabsChange} locale={locale} />
          </TabPanel>
          <TabPanel>
            <Standard locale={locale} />
          </TabPanel>
          <TabPanel>
            <Premium locale={locale} />
          </TabPanel>
        </TabPanels>
      </Tabs>
      <Spacer />
    </Col>
  );
}

const labels = {
  EN: {
    0: 'CHOOSE EXPERIENCE',
    1: ' STANDARD TICKET',
    2: ' PREMIUM TICKET',
  },
  FR: {
    0: 'CHOISIR SON EXPERIENCE',
    1: 'ENTREE STANDARD',
    2: 'ENTREE PREMIUM',
  },
};

export { EnrollSection };
