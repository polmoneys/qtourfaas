const EN = {
  title: 'CONTACT US',
  name: 'First name and surname',
  email: 'Email address',
  question: 'Your request',
  confirm: 'I consent to my submitted data being collected and stored.',
  button: 'Send message',
  success: 'Thank you for your message. Our team will stay in touch shortly.',
  error:
    "Unfortunately we can't process your request, please reach us at qliktourgeneve@cross-systems.com for more information.",
};

const FR = {
  title: 'NOUS CONTACTER',
  name: 'Nom et prénom',
  email: 'Email',
  question: 'Votre demande',
  confirm: "J'accepte que mes données soient collectées et stockées",
  button: 'Envoyer le message',
  success:
    'Merci pour votre message. Notre équipe reviendra vers vous prochainement.',
  error:
    "Malheureusement nous n'avons pas pu sauvegarder votre demande, veuillez nous contacter à qliktourgeneve@cross-systems.com pour obtenir plus d'informations.",
};

const TRANSLATIONS = {
  FR,
  EN,
};
export default TRANSLATIONS;
