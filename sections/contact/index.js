import { useEffect, useState } from 'react';
import axios from 'axios';
import { Action } from '../../units/action';
import { Col, Spacer } from '../../units/box';
import { useForm, Input, Form, CheckBox, isEmptyStr,hasArroba } from '../../units/forms';
import TRANSLATIONS from './locale';

const initialState = {
  email: '',
  fullName: '',
  agree: false,
  question: '',
};

function ContactSection({ locale = 'FR' }) {
  const [formStates, formActions] = useForm(() => initialState);
  const { awaiting, v, fail, ok, invalid } = formStates;

  const {
    updateForm,
    showFixes,
    setError,
    setSuccess,
    setAwaiting,
  } = formActions;

  async function persist(fields) {
    setAwaiting(true);
    try {
      const res = await axios.post('/api/contact', fields);
      if ((res.status = 200)) return setSuccess(true);
    } catch (error) {
      setError(true);
      setSuccess(false);
    } finally {
      setAwaiting(false);
    }
  }

  const update = (e) => {
    const { target, persist } = e;
    if (persist) e.persist();
    const { name, value } = target;
    updateForm((prev) => ({ ...prev, [name]: value }));
  };

  const action = () => {
    if (
      isEmptyStr(v.fullName) ||
      isEmptyStr(v.email) ||
      !hasArroba(v.email) ||
      isEmptyStr(v.question) ||
      !v.agree
    ) {
      showFixes(true);
    } else {
      const { agree, ...userDetails } = v;
      persist(userDetails);
    }
  };

  const translate = (goal) => TRANSLATIONS[locale][goal];

  if (fail) return <ContactError msg={translate('error')} />;
  if (ok) return <ContactSuccess msg={translate('success')} />;

  return (
    <Col tag="section" className="section-xs -blanc">
      <h2 className="h3 font-bold">{translate('title')}</h2>
      <Spacer />

      <Form id="contact-form" onSubmit={action}>
        <Input.Required
          initial={v.fullName}
          name="fullName"
          label={translate('name')}
          onChange={update}
          showFixes={invalid}
          enterkeyhint="next"
          locale={locale}
        />
        <Input.Email
          required
          initial={v.email}
          name="email"
          label={translate('email')}
          onChange={update}
          showFixes={invalid}
          enterkeyhint="next"
          locale={locale}
        />
        <Input.Required
          initial={v.question}
          name="question"
          label={translate('question')}
          onChange={update}
          showFixes={invalid}
          enterkeyhint="next"
          locale={locale}
        />
        <CheckBox
          initial={v.agree}
          name="agree"
          label="*"
          onChange={update}
          required
          showFixes={invalid}
        />
        <p className="font-s">{translate('confirm')}</p>
        <Spacer />

        <Action className="traced -blanc" disabled={awaiting} onPress={action}>
          {translate('button')}
        </Action>
      </Form>
    </Col>
  );
}

const ContactSuccess = ({ msg }) => (
  <section className="section-l -blanc">
    <Spacer size="xl" />
    <p className=" font-center">{msg}</p>
    <Spacer size="xl" />
  </section>
);

const ContactError = ({ msg }) => (
  <section className="section-l -blanc">
    <Spacer size="xl" />
    <p className="font-center">{msg}</p>
    <Spacer size="xl" />
  </section>
);

export { ContactSection };
