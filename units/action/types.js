import PropTypes from 'prop-types';

const TYPES = {
  defaultProps: {
    type: 'button',
    disabled: false,
  },
  propTypes: {
    children: PropTypes.any.isRequired,
    href: PropTypes.string,
    to: PropTypes.string,
    disabled: PropTypes.bool,
    type: PropTypes.string,
    onClick: PropTypes.func,
    target: PropTypes.oneOf([null, 'self', '_blank']),
  },
};

export default TYPES;
