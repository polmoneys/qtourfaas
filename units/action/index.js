/**
 * @polmoneys  #2020#
 * version 1.0.0
 *
 */
import { useRef } from 'react';
import Link from 'next/link';
import TYPES from './types';
import { useButton } from '@react-aria/button';

const Action = (props) => {
  const { className, to, href, target, ...rest } = props;
  const ref = useRef();
  const { buttonProps } = useButton(props, ref);

  const Tag = to ? Link : href ? 'a' : 'button';
  const rel = target === '_blank' ? 'noopener noreferrer' : undefined;
  const type = Tag === 'button' ? 'button' : undefined;
  const clss = ['action', className].filter(Boolean).join(' ');

  const { children, id = undefined, ...events } = rest;
  const forwardProps = { to, href, events, id };

  return (
    <Tag
      {...forwardProps}
      ref={ref}
      rel={rel}
      {...(Tag === 'button' && { ...buttonProps })}
      className={clss}
      type={type}
    >
      {children}
    </Tag>
  );
};

export { Action };

Action.propTypes = TYPES.propTypes;
Action.defaultProps = TYPES.defaultProps;
