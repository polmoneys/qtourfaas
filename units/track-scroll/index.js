import { PureComponent } from 'react';

class Track extends PureComponent {
  componentDidMount() {
    this.hasStartedObservation = false;
    this.beginObservation();
  }

  componentWillUnmount() {
    if (this.observer && typeof this.observer.disconnect === 'function') {
      this.observer.disconnect();
    }
  }

  beginObservation = () => {
    const {
      id,
      children,
      onIntersect,
      onlyFireOn,
      ...intersectionOptions
    } = this.props;

    this.observer = new window.IntersectionObserver(
      (entries) => {
        // While the IntersectionObserver API supports multiple entries, we'll
        // only ever have 1, since we're only observing a single node.
        const [entry] = entries;

        this.triggerCallbackIfNecessary(entry);
        this.hasStartedObservation = true;
      },
      {
        ...intersectionOptions,
        threshold: 0.1,
        //  rootMargin:,
        //root:document.querySelector('main')
      },
    );

    this.observer.observe(this.elem);
  };

  triggerCallbackIfNecessary = (entry) => {
    const { id, onIntersect } = this.props;

    onIntersect(id);
  };

  render() {
    const { id, className = undefined, style = {} } = this.props;
    const Tag = this.props.tag || 'div';

    return (
      <Tag
        id={id}
        style={style}
        className={className}
        ref={(htmlElement) => (this.elem = htmlElement)}
      >
        {this.props.children}
      </Tag>
    );
  }
}

export { Track };
