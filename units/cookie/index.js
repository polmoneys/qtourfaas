import { useEffect, useState } from 'react';
import { Action } from '../action';

function Cookie({ storageKey = 'gdrp', locale = 'FR' }) {
  const [dissmised, setStatus] = usePersistedState(storageKey, 'N');

  const close = () => {
    setStatus(false);
    setStatus('Y');
  };

  if (dissmised === 'Y') return null;
  return (
    <div id="cookie">
      <Action onClick={close} className="h2 action-transparent action-tight">
        &times;
      </Action>
      <p className="font-s">{t[locale]}</p>
    </div>
  );
}

const t = {
  FR:
    "Les cookies assurent le bon fonctionnement de nos services. En poursuivant votre navigation sur ce site, vous en acceptez l'utilisation.",
  EN:
    'Cookies ensure the smooth running of our services. By continuing to browse this site, you accept their use.',
};

export { Cookie };

function usePersistedState(key, defaultValue) {
  const [state, setState] = useState(
    () => JSON.parse(localStorage.getItem(key)) || defaultValue,
  );
  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(state));
  }, [key, state]);
  return [state, setState];
}
