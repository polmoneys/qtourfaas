const PROGRESS_OFFSET = 9;

const ProgressBar = ({ amount }) => (
  <div
    aria-hidden="true"
    className="progress-bar"
    style={{ width: `${amount * 100 + PROGRESS_OFFSET}%` }}
  />
);

export { ProgressBar };
