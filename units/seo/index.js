const SITE_CONSTANTS = {
  title: 'Qlik Day Geneva | Genève | 8 oct. 2020 | CROSS x QLIK',
  description:
    'Apprenez à libérer la valeur de la donnée, démocratisez son accès et faites en sorte qu’elle réponde enfin à vos interrogations métier avec nos experts data',
  keywords:
    'Data, Cross Systems, Qlick, Qlik day, Qlik analytics, Geneve, Suisse, Data revolution, Advanced analytics, Augmented Analytics',
  copyright: '© COPYRIGHT 2020 CROSS - GROUPE MICROPOLE. TOUS DROITS RÉSERVÉS.',
  contact: 'qliktourgeneve@cross-systems.com',
  url: 'https://qliktourgeneve.cross-systems.ch',
  img: '',
};

const Seo = () => {
  return (
    <>
      <meta charSet="utf-8" />
      <title>Qlik Day Geneva | Genève | 8 oct. 2020 | CROSS x QLIK</title>
      <meta
        name="title"
        content="Qlik Day Geneva | Genève | 8 oct. 2020 | CROSS x QLIK"
      />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link rel="canonical" href={SITE_CONSTANTS.url} />
      <meta property="og:locale" content="fr" />
      <meta name="description" content={SITE_CONSTANTS.description} />
      <meta name="keywords" content={SITE_CONSTANTS.keywords} />
      <meta name="contact" content={SITE_CONSTANTS.contact} />
      <meta name="copyright" content={SITE_CONSTANTS.copyright} />
      <meta name="og:url" content={SITE_CONSTANTS.url} />
      <meta name="og:title" content={SITE_CONSTANTS.title} />
      <meta name="og:description" content={SITE_CONSTANTS.description} />
      <meta name="og:type" content="website" />
      <meta name="twitter:title" content={SITE_CONSTANTS.title} />
      <meta name="twitter:description" content={SITE_CONSTANTS.description} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:url" content={SITE_CONSTANTS.url} />
      <meta name="twitter:image" content="/landscape.jpg" />
      <meta name="og:image" content="/landscape.jpg" />
      <meta name="googlebot" content="index,follow" />
      <meta name="robots" content="index,follow" />
      <meta name="rating" content="general" />
      <meta name="apple-mobile-web-app-capable" content="no" />
      <link rel="shortcut icon" href="/favicon.ico" />
    </>
  );
};

export { Seo };
