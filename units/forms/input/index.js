import React, { useEffect, useRef, useState } from 'react';
import { InputDefaultProps, InputPropTypes } from '../types';
import { InputLabel } from './label';
import { default as __, ShowIf } from '../utils';
import * as yup from 'yup';

function Input(props) {
  const { initial, name, onChange, showFixes, ...otherProps } = props;
  const [typing, setInputValue] = useState(initial);
  const [error, setError] = useState(false);
  const [showError, setShowError] = useState(showFixes);
  const inputElement = useRef();
  const [capsLockOn, setCapsLock] = useState(false);

  useEffect(() => {
    const { autofocus } = otherProps;
    if (autofocus) inputElement.current.focus();
  });

  useEffect(() => {
    if (showFixes) setShowError(true);
    else setShowError(false);
  }, [showFixes]);

  const { validation } = otherProps;

  useEffect(() => {
    if (validation) {
      validation
        .validate(typing)
        .then(() => {
          // eslint-disable-line
          setError(false);
        })
        .catch((ValidationError) => {
          setError({
            type: name,
            msg: ValidationError.message,
          });
        });
    }
  }, [typing, name, validation]);

  const onChangeUpdate = (e) => {
    e.persist();
    // disabled at codesandbox
    // setCapsLock(e.getModifierState("CapsLock"));
    setInputValue(e.target.value);
    onChange(e);
  };

  /*   const onPasteUpdate = e => {
    e.persist();
    const pasted = e.clipboardData.getData("text/plain");
    document.execCommand("insertHTML", false, pasted); // eslint-disable-line
  };
 */
  const {
    onBlur,
    onFocus,
    // onPaste,
    autocomplete = false,
    label,
    ...elementAttributes // like required or minlenght or inputmode or id or disabled
  } = otherProps;

  const inputClassNames = assignClassNames(props);
  const containerClassNames = error
    ? 'input-container error'
    : 'input-container';
  return (
    <div className={containerClassNames}>
      <InputLabel {...props} />

      <input
        ref={inputElement}
        value={typing}
        name={name}
        className={inputClassNames}
        autoComplete={autocomplete ? 'on' : 'off'}
        aria-required={otherProps.required || false}
        aria-label={otherProps.required ? 'required' : 'optional'}
        onChange={onChangeUpdate}
        onFocus={onFocus ? onFocus : __.noop()}
        onBlur={onBlur ? onBlur : __.noop()}
        // onPaste={onPaste ? onPaste : onPasteUpdate}
        {...elementAttributes}
      />

      <ShowIf condition={error && showError}>
        <p input-error="" aria-live="polite">
          {translateValidationMessages(error.msg, props.locale)}
        </p>
      </ShowIf>
    </div>
  );
}

// TODO: REFRACTOR TRANSLATIONS AS YUP SCHEMAS
function translateValidationMessages(msg, locale = 'FR') {
  if (!msg || msg.length === 0) return null;
  if (msg[0] === 'V') {
    const email = {
      FR: 'Veuillez entrer une adresse email valide',
      EN: 'Please enter a valid email address',
    };
    return email[locale];
  } else {
    const notEmpty = {
      EN: 'This field is mandatory',
      FR: 'Ce champ est obligatoire',
    };
    return notEmpty[locale];
  }
  return null;

  // return
}

function assignClassNames(props) {
  return [props.className, props.s && 'input-s', props.xs && 'input-xs']
    .filter(Boolean)
    .join(' ');
}

Input.Required = (props) => (
  <Input validation={freeTextValidation} required {...props} />
);
Input.Email = (props) => (
  <Input
    type="email"
    inputMode="email"
    validation={emailValidation}
    {...props}
  />
);

Input.displayName = 'DefaultsInput';
Input.propTypes = InputPropTypes;
Input.defaultProps = InputDefaultProps;

export { Input };

const freeTextValidation = yup.string().required('Ce champ est obligatoire');

const emailValidation = yup
  .string()
  .email('Veuillez entrer une adresse email valide')
  .required('Ce champ est obligatoire');
