import PropTypes from 'prop-types';
import { default as __ } from './utils';

const FormPropTypes = {
  children: PropTypes.any,
  feedback: PropTypes.object,
};

const FormDefaultProps = {
  feedback: {},
  children: null,
};

export { FormPropTypes, FormDefaultProps };

const InputPropTypes = {
  label: PropTypes.string.isRequired,
  onBlur: PropTypes.func,
  onFocus: PropTypes.func,
  onPaste: PropTypes.func,
  type: PropTypes.string,
};

const InputDefaultProps = {
  label: '',
  onBlur: __.noop(),
  onFocus: __.noop(),
  onPaste: __.noop(),
  type: 'text',
};

export { InputDefaultProps, InputPropTypes };

const CheckBoxPropTypes = {
  // label: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.node,
  //   PropTypes.string,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]).isRequired,
  // icon: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.node,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]),
  disabled: PropTypes.bool,
};

const CheckBoxDefaultProps = {
  disabled: false,
  required: false,
  icon: false,
  label: '',
};

export { CheckBoxPropTypes, CheckBoxDefaultProps };

const RadioPropTypes = {
  //   label: PropTypes.oneOf([PropTypes.element, PropTypes.node, PropTypes.string])
  //     .isRequired,
  labelGroup: PropTypes.string,
};

const RadioDefaultProps = {
  label: '',
  labelGroup: '',
};

export { RadioPropTypes, RadioDefaultProps };

const SwitchPropTypes = {
  label: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.node,
    PropTypes.string,
  ]),
  onColor: PropTypes.string,
  offColor: PropTypes.string,
  // onIcon: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]),
  // offIcon: PropTypes.oneOfType([
  //   PropTypes.element,
  //   PropTypes.instanceOf(PropTypes.Bool)
  // ]),
  offCb: PropTypes.func,
  onCb: PropTypes.func,
};

const SwitchDefaultProps = {
  label: '',
  onColor: '#ff5555',
  offColor: '#242424',
  onIcon: false,
  offIcon: false,
  offCb: __.noop(),
  onCb: __.noop(),
};
export { SwitchPropTypes, SwitchDefaultProps };
