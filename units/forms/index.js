import React, { useState } from 'react';

import { Input } from './input';
import { CheckBox } from './checkbox';

function Form(props) {
  const { children, ...extras } = props;
  return <form {...extras}>{children}</form>;
}

const FormField = (props) => (
  <div {...props} className="flex-landscape form-field">
    {props.children}
  </div>
);

function useForm(initialState) {
  const [formValues, updateForm] = useState(initialState);
  const [invalid, showFixes] = useState(false); // show user validation message
  const [awaiting, setAwaiting] = useState(false); // disable form/buttons
  const [success, setSuccess] = useState(false); // 👍
  const [o_O, setError] = useState(false); // 👎

  return [
    {
      v: formValues,
      awaiting,
      fail: o_O,
      ok: success,
      invalid,
    },
    {
      updateForm,
      showFixes,
      setError,
      setSuccess,
      setAwaiting,
    },
  ];
}

function isEmptyStr(str) {
  return str.trim().length < 1;
}
function hasArroba(str){
  return str.includes('@');
}
export { Form, FormField, Input, CheckBox, isEmptyStr,hasArroba, useForm };
