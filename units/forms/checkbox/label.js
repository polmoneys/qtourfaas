import React from 'react';

const CheckboxIco = ({ checked }) => (
  <svg
    style={{ opacity: checked ? 1 : 0 }}
    viewBox="-8 -6 32 32"
    width="24"
    height="24"
    fill={checked ? '#121212' : '#f4f4f4'}
  >
    <path d="M16.62 6.21a1 1 0 0 0-1.41.17l-7 9-3.43-4.18a1 1 0 1 0-1.56 1.25l4.17 5.18a1 1 0 0 0 .78.37 1 1 0 0 0 .83-.38l7.83-10a1 1 0 0 0-.21-1.41z" />
    <path d="M21.62 6.21a1 1 0 0 0-1.41.17l-7 9-.61-.75-1.26 1.62 1.1 1.37a1 1 0 0 0 .78.37 1 1 0 0 0 .78-.38l7.83-10a1 1 0 0 0-.21-1.4z" />
    <path d="M8.71 13.06L10 11.44l-.2-.24a1 1 0 0 0-1.43-.2 1 1 0 0 0-.15 1.41z" />
  </svg>
);

const CheckboxLabel = ({ checked, name }) => <label htmlFor={name}>*</label>;
export { CheckboxLabel, CheckboxIco };
