import React, { useState, useEffect } from 'react';
import { default as __, ShowIf } from '../utils';
import { CheckBoxPropTypes, CheckBoxDefaultProps } from '../types';
import { CheckboxLabel } from './label';

function CheckBox({
  initial,
  name,
  onChange,
  required,
  showFixes,
  ...otherProps
}) {
  const [selectedCheckBox, setSelectedValue] = useState(initial);
  const [error, setError] = useState(() =>
    required && !initial ? true : false,
  );
  const [showError, setShowError] = useState(false);

  useEffect(() => {
    setShowError(showFixes);
  }, [showFixes]);
  const onChangeUpdate = (e) => {
    setSelectedValue(e.target.checked);
    onChange({ target: { name, value: e.target.checked } });
  };

  const { disabled, label } = otherProps;

  const containerClassNames = showError
    ? 'checkbox-container error'
    : 'checkbox-container ';
  return (
    <>
      <div className={containerClassNames}>
        <svg
          width="24"
          height="24"
          viewBox="0 0 24 24"
          style={{
            left: '4px',
            display: selectedCheckBox ? 'block' : 'none',
          }}
        >
          <path d="M9.86 18a1 1 0 0 1-.73-.32l-4.86-5.17a1 1 0 1 1 1.46-1.37l4.12 4.39 8.41-9.2a1 1 0 1 1 1.48 1.34l-9.14 10a1 1 0 0 1-.73.33z" />
        </svg>

        <input
          name={name}
          value={name}
          checked={selectedCheckBox}
          disabled={disabled}
          type="checkbox"
          onChange={onChangeUpdate}
        />
        <CheckboxLabel name={name} label={label} checked={selectedCheckBox} />
      </div>
      <ShowIf condition={error && showError}>
        <p input-error="" aria-live="polite">
          {error.msg}
        </p>
      </ShowIf>
    </>
  );
}

CheckBox.displayName = 'DefaultsCheckBox';
CheckBox.propTypes = CheckBoxPropTypes;
CheckBox.defaultProps = CheckBoxDefaultProps;

export { CheckBox };
