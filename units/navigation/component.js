/**
 * @polmoneys  #2020#
 * Navigation with resize observer  1.0.0
 */

import React, { useCallback, useRef, useState, useEffect } from 'react';
import { animate, timeline } from './animations';
import { useResizeObserver } from './resize-observer';
import { Ico } from './ico';
import TYPES from './types';

const MQ_CHANGE = 960; // 🚨  sync with css

function Navigation(props) {
  const navRef = useRef(null);
  const { width } = useResizeObserver(navRef);

  const [isSidebarOpen, setStatusSidebar] = useState(false);
  const [isAnimating, setProgress] = useState(false);
  const sidebarRef = useRef();
  const menuButtonRef = useRef();

  const [isLandscape, setOrientation] = useState(true);

  useEffect(() => {
    if (!width || width === 0) return;
    if (width >= MQ_CHANGE) {
      setOrientation(true);
      setStatusSidebar(false);
    } else {
      setOrientation(false);
    }
  }, [width]);

  // IF !LANDSCAPE... SIDEBAR !

  const open = () => {
    timeline()
      .then(() =>
        animate(menuButtonRef, {
          opacity: 0,
        }),
      )
      .then(() =>
        animate(sidebarRef, {
          transform: 'translateX(0)',
        }),
      )
      .then(() => {
        setStatusSidebar((val) => !val);
        setProgress(false);
      });
  };

  const close = () => {
    timeline()
      .then(() =>
        animate(sidebarRef, {
          transform: 'translateX(-300px)',
        }),
      )
      .then(() =>
        animate(menuButtonRef, {
          opacity: 1,
        }),
      )
      .then(() => {
        setStatusSidebar((val) => !val);
        setProgress(false);
      });
  };

  const toggle = useCallback(() => {
    if (isAnimating) return;
    setProgress(true);
    isSidebarOpen ? close() : open();
  }, [isAnimating, isSidebarOpen]);

  useEffect(() => {
    window.addEventListener('keydown', keyboardHandle);
    return () => {
      window.removeEventListener('keydown', keyboardHandle);
    };
  });

  function keyboardHandle(e) {
    if (width > MQ_CHANGE) return;
    if (e.key !== 'Escape') return;
    toggle();
  }

  const getMenuButtonProps = () => ({
    'aria-haspopup': true,
    'aria-expanded': isSidebarOpen,
  });

  // END SIDEBAR LOGIC

  const { children, portraitIcon, portraitCta } = props;
  const clss = ['flex', 'main-end', 'cross-center', isLandscape && '-light']
    .filter(Boolean)
    .join(' ');

  const landscapeContent = children && children({ close, isLandscape });
  const portraitContent = (
    <>
      <button
        {...getMenuButtonProps()}
        ref={menuButtonRef}
        type="button"
        id="menu-button"
        onClick={toggle}
        className="self-push-right"
      >
        {portraitIcon ? portraitIcon : <Ico status={isSidebarOpen} />}
      </button>
      {!isSidebarOpen && portraitCta}
    </>
  );

  return (
    <>
      <nav
        className={clss}
        ref={navRef}
        style={{
          opacity: !width ? 0 : width === 0 ? 0 : 1,
        }}
      >
        {isLandscape ? landscapeContent : portraitContent}
      </nav>
      {!isLandscape && (
        <div ref={sidebarRef} className="sidebar">
          {children && children({ close, isLandscape })}
        </div>
      )}
    </>
  );
}

Navigation.propTypes = TYPES.propTypes;
Navigation.defaultProps = TYPES.defaultProps;

export { Navigation };
