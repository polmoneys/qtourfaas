import PropTypes from 'prop-types';

const TYPES = {
  defaultProps: {
    portraitCta: undefined,
    children: undefined,
    portraitIcon: undefined,
  },
  propTypes: {
    children: PropTypes.any.isRequired,
    portraitCta: PropTypes.any,
    portraitIcon: PropTypes.any,
  },
};

export default TYPES;
