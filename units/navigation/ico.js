import React from 'react';

const Ico = ({ status, fillOff = 'var(--violet)', fillOn = 'transparent' }) => (
  <svg viewBox={'0 -16 66 66'} width="66" height="66">
    <rect fill={status ? fillOn : fillOff} x="0" y="0" width="60" height="8" />
    <rect fill={status ? fillOn : fillOff} x="0" y="14" width="60" height="8" />
    <rect x="0" y="28" width="60" height="8" fill={status ? fillOn : fillOff} />
  </svg>
);

export { Ico };
