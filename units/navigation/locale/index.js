const EN = {
  nav1: 'EXPERIENCE',
  nav2: 'USEFUL INFORMATION',
  nav3: 'CONTACT',
  button: 'REGISTER',
};

const FR = {
  nav1: 'EXPERIENCE',
  nav2: 'PRATIQUE',
  nav3: 'CONTACT',
  button: "S'INSCRIRE",
};

const TRANSLATION = {
  FR,
  EN,
};
export default TRANSLATION;
