import dynamic from 'next/dynamic';
import Link from 'next/link';

import { Action } from '../action';
import { Pic } from '../pic';
import { Row, Spacer } from '../box';
import TRANSLATIONS from './locale';
import { Navigation } from './component';
import { SmartLink } from './pathname-link';

const Header = (props) => {
  const { locale = 'FR', current } = props;
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <Navigation
      portraitCta={
        <Action
          id="sidebar-cta"
          href="#experience"
          className=" self-push-left -blanc"
        >
          {translate('button')}
        </Action>
      }
    >
      {({ close, isLandscape }) => (
        <>
          <button type="button" onClick={close} id="close-button">
            &times;
          </button>
          {isLandscape && (
            <div className="self-push-right flex row-gap -xl cross-center ">
              <a
                target="_blank"
                rel="noopener noreferer"
                href="https://www.cross-systems.ch/"
                rel="noopener noreferrer"
              >
                <Pic alt="cross systems logo" url="/cross-logo.png" />
              </a>
              <a
                target="_blank"
                rel="noopener noreferer"
                href="https://www.qlik.com/"
                rel="noopener noreferrer"
              >
                <Pic alt="qlik logo" url="/qlik-logo.png" />
              </a>
            </div>
          )}

          <Action
            className={
              isLandscape
                ? assignActiveClass(current, 'info')
                : 'action-transparent nav-action-ellipsis'
            }
            href="#info"
          >
            QLIK DAY GENEVA
          </Action>
          <Action
            className={
              isLandscape
                ? assignActiveClass(current, 'experience')
                : 'action-transparent'
            }
            href="#experience"
          >
            {translate('nav1')}
          </Action>
          <Action
            className={
              isLandscape
                ? assignActiveClass(current, 'pratique')
                : 'action-transparent'
            }
            href="#pratique"
          >
            {translate('nav2')}
          </Action>

          <Action
            className={
              isLandscape
                ? assignActiveClass(current, 'contact')
                : 'action-transparent'
            }
            href="#contact"
          >
            {translate('nav3')}
          </Action>
          <Action href="#experience" className=" -blanc">
            {translate('button')}
          </Action>

          <Row className="locale-switcher">
            <SmartLink activeClassName="link-underline" href="/">
              <a className="action action-transparent">FR</a>
            </SmartLink>
            <SmartLink activeClassName="link-underline" href="/en">
              <a className="action action-transparent">EN</a>
            </SmartLink>
          </Row>
          {!isLandscape && (
            <>
              <Spacer />
              <Action
                className="action-tight action-transparent"
                href="https://www.cross-systems.ch/"
                rel="noopener noreferrer"
              >
                <Pic alt="cross systems logo" url="/cross-logo-white.png" />
              </Action>
              <Action
                className="action-tight action-transparent"
                href="https://www.qlik.com/"
                rel="noopener noreferrer"
              >
                <Pic alt="qlik logo" url="/qlik-white.png" />
              </Action>
            </>
          )}
        </>
      )}
    </Navigation>
  );
};

function assignActiveClass(active, goal) {
  if (active === goal && goal === 'info')
    return 'nav-action-ellipsis action-transparent -violet font-bold ';
  if (goal === 'info') return 'nav-action-ellipsis action-transparent';
  if (active === goal) return 'action-transparent -violet font-bold';
  return 'action-transparent ';
}

export { Header };
