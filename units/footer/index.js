import { Ico } from '../icon';
import Link from 'next/link';
import { ColToRow, Spacer } from '../../units/box';
import TRANSLATIONS from './locale';

function Footer({ children, locale = 'FR' }) {
  const translate = (goal) => TRANSLATIONS[locale][goal];

  return (
    <footer className="pxy _noir -blanc">
      <Spacer size="l" />

      {children}
      <Spacer size="l" />

      <ColToRow className="row-gap -xl main-center cross-center">
        <a
          href="https://www.linkedin.com/company/cross-systems/?viewAsMember=true"
          target="_blank"
          rel="noopener noreferer"
        >
          {' '}
          <Ico color="var(--blanc)" />
        </a>
        <a
          href="https://www.linkedin.com/company/cross-systems/?viewAsMember=true"
          className="font-s"
          target="_blank"
          rel="noopener noreferer"
        >
          {translate('followus')}
        </a>
        <Spacer.Portrait size="l" />

        <p className="font-s font-center">
          {' '}
          &#169; COPYRIGHT 2020 CROSS - GROUPE MICROPOLE.{' '}
          {translate('copyright')}
        </p>
        <Spacer.Portrait size="l" />

        <p className="font-center font-s">
          <Link href="/legal">
            <a>{translate('legal')}</a>
          </Link>
        </p>
      </ColToRow>
      <Spacer size="xl" />
      <Spacer />
    </footer>
  );
}

export { Footer };
