const EN = {
  legal: 'Legal information',
  followus: 'Follows us',
  copyright: 'All rights reserved',
};

const FR = {
  legal: 'Informations légales',
  followus: 'Nous suivre',
  copyright: 'TOUS DROITS RÉSERVÉS',
};

export default { EN, FR };
