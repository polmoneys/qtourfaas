/**
 * @polmoneys  #2020#
 */
import React, { cloneElement, isValidElement } from 'react';
import TYPES from './types';

const { map } = React.Children;

const Box = ({ children, ...options }) => {
  /* eslint-disable no-undef */
  if (process.env.NODE_ENV === 'development') {
    if (children === null) {
      console.warn('A Box with no children makes sense ? ');
    }
  }
  /* eslint-enable */

  const {
    tag,
    gap,
    gapDirection,
    className,
    defaultClassnames,
    ...propsLeft
  } = options;
  const Tag = tag;
  const clss = [className, defaultClassnames].filter(Boolean).join(' ');
  return (
    <Tag className={clss} {...propsLeft}>
      {map(children, (c, idx) => {
        if (!isValidElement(c) || typeof c.type === 'string') {
          return c;
        }

        const isLast = isLastItem(children, idx);
        return cloneElement(c, {
          // offset: isLast ? 0 : gap,
          // offsetDirection:
          //   gapDirection === "row"
          //     ? "row"
          //     : gapDirection === "grid"
          //     ? "both"
          //     : "col"
        });
      })}
    </Tag>
  );
};

const Row = (props) => (
  <Box defaultClassnames="flex" gapDirection="row" {...props} />
);
const Col = (props) => (
  <Box defaultClassnames="col flex" gapDirection="col" {...props} />
);
const Grid = (props) => (
  <Box defaultClassnames="grid flex" gapDirection="grid" {...props} />
);
const ColToRow = (props) => (
  <Box defaultClassnames="flex-landscape" gapDirection="grid" {...props} />
);

Box.propTypes = TYPES.propTypes;
Box.defaultProps = TYPES.defaultProps;

export { Row, Col, Grid, ColToRow };

function isLastItem(arr, index) {
  return index === arr.length - 1;
}

const SPACER_SIZES = {
  xs: '8px',
  s: '12px',
  default: '16px',
  l: '20px',
  xl: '40px',
};

function Spacer({
  size = 'default',
  portraitOnly = false,
  landscapeOnly = false,
  matchesMqOnly = false,
}) {
  const space = SPACER_SIZES[size];
  const clss = [
    'flex',
    'self-grow',
    portraitOnly && 'portrait-only',
    landscapeOnly && 'landscape-only',
    matchesMqOnly && 'mq-only',
  ]
    .filter(Boolean)
    .join(' ');

  return (
    <div
      aria-hidden="true"
      className={clss}
      style={{ height: space }}
      // vs. --spacer:`${space}px
    />
  );
}

Spacer.Portrait = (props) => <Spacer {...props} portraitOnly />;
Spacer.Landscape = (props) => <Spacer {...props} landscapeOnly />;
Spacer.MQ = (props) => <Spacer {...props} matchesMqOnly />;

export { Spacer };
