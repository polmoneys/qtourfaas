import PropTypes from 'prop-types';

const TYPES = {
  propTypes: {
    tag: PropTypes.string,
    theme: PropTypes.string,
    children: PropTypes.any,
    gapDirection: PropTypes.oneOf(['row', 'col', 'grid']),
  },
  defaultProps: {
    tag: 'div',
    gapDirection: 'row',
  },
};

export default TYPES;
