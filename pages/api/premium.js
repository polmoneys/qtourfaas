const Airtable = require('airtable');

const base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY,
}).base('appUdohCKNxuBjmim');
const premiumTable = base('premiumTable');

export default async (req, res) => {
  const { body } = req;
  const { statusCode } = res;
  try {
    await premiumTable.create(body);
    res.status(200).json({ message: 'success' });
  } catch (o_O) {
    res.status(o_O.statusCode);
  } finally {
    res.end();
  }
};
