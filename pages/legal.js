import Link from 'next/link';
import { Action } from '../units/action';
import { ColToRow, Row, Spacer } from '../units/box';
import { Pic } from '../units/pic';

export default function Privacy() {
  return (
    <>
      <Spacer size="xl" />
      <Spacer size="xl" />

      <Row tag="nav" className="landscape -light main-end cross-center pxy">
        <div className="self-push-right flex row-gap -xl cross-center">
          <a
            target="_blank"
            rel="noopener noreferer"
            href="https://www.cross-systems.ch/"
            rel="noopener noreferrer"
          >
            <Pic alt="cross systems logo" url="/cross-logo.png" />
          </a>
          <a
            target="_blank"
            rel="noopener noreferer"
            href="https://www.qlik.com/"
            rel="noopener noreferrer"
          >
            <Pic alt="qlik logo" url="/qlik-logo.png" />
          </a>
        </div>
        <Action className="_violet -blanc" href="/">
          QLIK DAY GENEVA
        </Action>
      </Row>
      <main>
        <section className="section-s flex col">
          <ColToRow className="main-between cross-center">
            <h1 className="h1 font-bold">QLIK DAY GENEVA: 404</h1>
            <p className="font-bold -violet"> #qlikdaygeneva</p>
          </ColToRow>
          <Spacer />
          <p>
            SIÈGE SOCIAL : <br aria-hidden="true" />
            Cross Systems
            <br aria-hidden="true" />
            Route des Acacias 45B
            <br aria-hidden="true" />
            CH-1227 Les Acacias
            <br aria-hidden="true" />
            SUISSE
          </p>

          <p> Tél. : + 41 (0)22 308 48 60</p>

          <p> E-mail : contact@cross-systems.ch</p>
          <p>
            {' '}
            Directeur de la publication : Fabrice PERRIN (Directeur Général)
          </p>
          <Spacer size="xl" />
          <Action
            href="/"
            className="action-landscape-max-width _violet -blanc"
          >
            Accueil
          </Action>
        </section>
      </main>
    </>
  );
}
