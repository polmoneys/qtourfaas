// import ResizeObserver from 'resize-observer-polyfill';  this does not load on EDGE
import '../styles/index.css';

import '../sections/hero/hero.css';
import '../sections/enroll/enroll.css';
import '../sections/event/event.css';
import '../units/navigation/navigation.css';

import '../units/action/action.css';
import '../units/forms/styles.css';
import '../units/pic/pic.css';
import '../units/progress/progress.css';
import '../units/cookie/cookie.css';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
