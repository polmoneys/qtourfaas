import dynamic from 'next/dynamic';
import Head from 'next/head';
import { useState } from 'react';
import { useScrollPercentage } from 'react-scroll-percentage';

import { HeroSection } from '../sections/hero';
import { EventSection } from '../sections/event';
import { EnrollSection } from '../sections/enroll';
import { ContactSection } from '../sections/contact';
import { VenueSection } from '../sections/event/venue';
import { AboutSection } from '../sections/event/about';

// import { Header } from "../units/header";
import { Footer } from '../units/footer';
import { Track } from '../units/track-scroll';

import { Header } from '../units/navigation';
import { ProgressBar } from '../units/progress';

const SafeSSRCoookie = dynamic(
  () => import('../units/cookie/index.js').then((mod) => mod.Cookie),
  {
    ssr: false,
  },
);
export default function Home() {
  const [currentSection, setSection] = useState('info');
  const [ref, scrollProgress] = useScrollPercentage({
    threshold: 0,
  });

  function changeSection(entry) {
    setSection(entry);
  }

  return (
    <>
      <Header current={currentSection} />
      <ProgressBar amount={scrollProgress} />

      <SafeSSRCoookie />

      <div ref={ref} id="scroll-ref">
        <HeroSection />

        <main>
          <Track
            id="info"
            className="scroll-adjust-position"
            onIntersect={(id) => changeSection(id)}
          >
            <AboutSection />
          </Track>

          <EventSection />

          <Track id="experience" onIntersect={(id) => changeSection(id)}>
            <EnrollSection />
          </Track>

          <Track
            id="pratique"
            className="scroll-adjust-position"
            onIntersect={(id) => changeSection(id)}
          >
            <VenueSection />
          </Track>

          <Track
            id="contact"
            className="_noir"
            onIntersect={(id) => changeSection(id)}
          >
            <ContactSection />
          </Track>
        </main>
      </div>
      <Footer />
    </>
  );
}
