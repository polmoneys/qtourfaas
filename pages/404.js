import Link from 'next/link';
import { Action } from '../units/action';
import { ColToRow, Row, Spacer } from '../units/box';
import { Pic } from '../units/pic';

export default function Custom404() {
  return (
    <>
      <Spacer size="xl" />
      <Spacer size="xl" />

      <Row tag="nav" className="landscape -light main-end cross-center pxy">
        <div className="self-push-right flex row-gap -xl cross-center">
          <a
            target="_blank"
            rel="noopener noreferer"
            href="https://www.cross-systems.ch/"
            rel="noopener noreferrer"
          >
            <Pic alt="cross systems logo" url="/cross-logo.png" />
          </a>
          <a
            target="_blank"
            rel="noopener noreferer"
            href="https://www.qlik.com/"
            rel="noopener noreferrer"
          >
            <Pic alt="qlik logo" url="/qlik-logo.png" />
          </a>
        </div>
        <Action className="_violet -blanc" href="/">
          QLIK DAY GENEVA
        </Action>
      </Row>
      <main>
        <section className="section-s flex col">
          <ColToRow className="main-between cross-center">
            <h1 className="h1 font-bold">QLIK DAY GENEVA: 404</h1>
            <p className="font-bold -violet"> #qlikdaygeneva</p>
          </ColToRow>
          <Spacer />
          <p>
            Veuillez nous excuser, la page demandée n'existe pas ou plus, ou
            l'URL que vous avez saisie est erronée.
          </p>
          <Spacer size="xl" />
          <Action
            href="/"
            className="action-landscape-max-width _violet -blanc"
          >
            Accueil
          </Action>
        </section>
      </main>
    </>
  );
}
