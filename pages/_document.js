import Document, { Html, Head, Main, NextScript } from 'next/document';
import { Seo } from '../units/seo';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <Html lang="fr">
        <Head>
          <Seo />
          <link
            href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap"
            rel="stylesheet"
          />
          <script
            src="https://widget.cloudinary.com/v2.0/global/all.js"
            type="text/javascript"
          >
            {' '}
          </script>
        </Head>
        <body>
          <div className="unsupported-experience" aria-hidden="true">
            <p>
              Your browser is not supported. Please choose an alternative or use
              you mobile phone.{' '}
            </p>
          </div>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
